<?php namespace Abduhamid\User;

use Backend;
use App;
use Event;
use System\Classes\PluginBase;

/**
 * User Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'User',
            'description' => 'No description provided yet...',
            'author'      => 'Abduhamid',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        if (App::runningInBackend()) {
            Event::listen('backend.list.extendColumns', function($widget) {
                if ($widget->getController() instanceof  \RainLab\User\Controllers\Users) {
                    $widget->addColumns([
                        'group' => [
                            'label'   => 'Groups',
                            'relation' => 'groups',
                            'select' => 'name',

                        ]
                    ]);
                }

            });
        }
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Abduhamid\User\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'abduhamid.user.some_permission' => [
                'tab' => 'User',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'user' => [
                'label'       => 'User',
                'url'         => Backend::url('abduhamid/user/users'),
                'icon'        => 'icon-leaf',
                'permissions' => ['abduhamid.user.*'],
                'order'       => 500,
            ],
        ];
    }
}
